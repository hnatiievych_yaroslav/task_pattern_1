package com.hnatiievych.controller;

import com.hnatiievych.model.City;
import com.hnatiievych.model.PizzaConfig;

import java.util.Arrays;

public class ControllerImp implements Controller {
    @Override
    public City findCityByString(String str) {
        return Arrays.stream(City.values()).filter(v -> v.getName().equals(str)).findFirst().orElse(null);
    }

    @Override
    public void createPizza(City city, String typePizza) {
        PizzaConfig.choosePizza(city, typePizza).prepare();
    }
}
