package com.hnatiievych.controller;

import com.hnatiievych.model.City;

public interface Controller {
    void createPizza(City city, String typePizza);
    City findCityByString(String str);
}
