package com.hnatiievych.view;

import com.hnatiievych.controller.Controller;
import com.hnatiievych.controller.ControllerImp;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {

    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);
    private static Logger logger = LogManager.getLogger(MyView.class);

    public MyView() {
        controller = new ControllerImp();
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - Order Pizza");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
    }

    private void pressButton1() {
        logger.info("write you city, like Lviv, Kyiv, Dnipro");
        String city = input.nextLine();
        logger.info("Choose pizza you want to order. Like CHEESE, VEGGIE, CLAM, PEPPERONI");
        String typePizza = input.nextLine();
        controller.createPizza(controller.findCityByString(city), typePizza);

    }

    private void pressButton2() {

    }

    private void pressButton3() {

    }


    //-------------------------------------------------------------------------

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
