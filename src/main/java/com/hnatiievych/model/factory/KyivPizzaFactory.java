package com.hnatiievych.model.factory;

import com.hnatiievych.model.Pizza;
import com.hnatiievych.model.impl.kyiv.KyivCheesePizza;
import com.hnatiievych.model.impl.kyiv.KyivClamPizza;
import com.hnatiievych.model.impl.kyiv.KyivPepperoniPizza;
import com.hnatiievych.model.impl.kyiv.KyivVeggiePizza;

public class KyivPizzaFactory implements PizzaFactory {
    public Pizza cheesePizza() {
        return new KyivCheesePizza();
    }

    public Pizza veggiePizza() {
        return new KyivVeggiePizza();
    }

    public Pizza clamPizza() {
        return new KyivClamPizza();
    }

    public Pizza pepperoniPizza() {
        return new KyivPepperoniPizza();
    }
}
