package com.hnatiievych.model.factory;

import com.hnatiievych.model.Pizza;
import com.hnatiievych.model.impl.dnipro.DniproCheesePizza;
import com.hnatiievych.model.impl.dnipro.DniproClamPizza;
import com.hnatiievych.model.impl.dnipro.DniproPepperoniPizza;
import com.hnatiievych.model.impl.dnipro.DniproVeggiePizza;

public class DniproPizzaFactory implements PizzaFactory {
    public Pizza cheesePizza() {
        return new DniproCheesePizza();
    }

    public Pizza veggiePizza() {
        return new DniproVeggiePizza();
    }

    public Pizza clamPizza() {
        return new DniproClamPizza();
    }

    public Pizza pepperoniPizza() {
        return new DniproPepperoniPizza();
    }
}
