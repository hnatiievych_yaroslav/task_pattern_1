package com.hnatiievych.model.factory;

import com.hnatiievych.model.Pizza;
import com.hnatiievych.model.impl.lviv.LvivCheesePizza;
import com.hnatiievych.model.impl.lviv.LvivClamPizza;
import com.hnatiievych.model.impl.lviv.LvivPepperoniPizza;
import com.hnatiievych.model.impl.lviv.LvivVeggiePizza;

public class LvivPizzaFactory implements PizzaFactory {

    public Pizza cheesePizza() {
        return new LvivCheesePizza();
    }

    public Pizza veggiePizza() {
        return new LvivVeggiePizza();
    }

    public Pizza clamPizza() {
        return new LvivClamPizza();
    }

    public Pizza pepperoniPizza() {
        return new LvivPepperoniPizza();
    }
}
