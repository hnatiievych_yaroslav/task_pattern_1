package com.hnatiievych.model.factory;

import com.hnatiievych.model.Pizza;

public interface PizzaFactory {
    Pizza cheesePizza();
    Pizza veggiePizza();
    Pizza clamPizza();
    Pizza pepperoniPizza();
}
