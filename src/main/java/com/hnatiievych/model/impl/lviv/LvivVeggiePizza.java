package com.hnatiievych.model.impl.lviv;

import com.hnatiievych.model.VeggiePizza;

public class LvivVeggiePizza extends LvivPizza implements VeggiePizza {


    public void prepare() {
        System.out.println("prepare lviv veggie pizza");
    }

    public void bake() {
        System.out.println("bake lviv veggie pizza");
    }
}
