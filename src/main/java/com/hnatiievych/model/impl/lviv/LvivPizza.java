package com.hnatiievych.model.impl.lviv;

import com.hnatiievych.model.Pizza;
import com.hnatiievych.model.Toppings;

import java.util.List;

public abstract class LvivPizza implements Pizza {
    private String dough;
    private String sauce;
    List<Toppings> toppings;

    public abstract void prepare();

    public abstract void bake();

    public void cut() {
        System.out.println("cut Lviv  pizza");
    }

    public void box() {
        System.out.println("box Lviv  pizza");
    }
}
