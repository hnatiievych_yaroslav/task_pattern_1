package com.hnatiievych.model.impl.lviv;

import com.hnatiievych.model.PepperoniPizza;

public class LvivPepperoniPizza extends LvivPizza implements PepperoniPizza {

    public void prepare() {
        System.out.println("prepare lviv pepperoni pizza");
    }

    public void bake() {
        System.out.println("bake lviv pepperoni pizza");
    }

}
