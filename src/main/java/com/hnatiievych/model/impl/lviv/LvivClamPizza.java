package com.hnatiievych.model.impl.lviv;

import com.hnatiievych.model.ClamPizza;

public class LvivClamPizza extends LvivPizza implements ClamPizza {

    public void prepare() {
        System.out.println("prepare lviv clam pizza");
    }

    public void bake() {
        System.out.println("bake lviv clam pizza");
    }
}
