package com.hnatiievych.model.impl.lviv;

import com.hnatiievych.model.CheesePizza;

public class LvivCheesePizza extends LvivPizza implements CheesePizza {


    public void prepare() {
        System.out.println("prepare lviv cheese pizza");
    }

    public void bake() {
        System.out.println("bake lviv cheese pizza");
    }
}