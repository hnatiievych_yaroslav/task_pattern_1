package com.hnatiievych.model.impl.kyiv;

import com.hnatiievych.model.VeggiePizza;

public class KyivVeggiePizza extends KyivPizza implements VeggiePizza {


    public void prepare() {
        System.out.println("prepare Kyiv veggie pizza");
    }

    public void bake() {
        System.out.println("bake Kyiv veggie pizza");
    }
}
