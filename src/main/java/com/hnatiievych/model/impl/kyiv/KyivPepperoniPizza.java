package com.hnatiievych.model.impl.kyiv;

import com.hnatiievych.model.PepperoniPizza;

public class KyivPepperoniPizza extends KyivPizza implements PepperoniPizza {


    public void prepare() {
        System.out.println("prepare Kyiv pepperoni pizza");
    }

    public void bake() {
        System.out.println("bake Kyiv pepperoni pizza");
    }
}
