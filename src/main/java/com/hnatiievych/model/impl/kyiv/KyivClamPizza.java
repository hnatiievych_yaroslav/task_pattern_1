package com.hnatiievych.model.impl.kyiv;

import com.hnatiievych.model.ClamPizza;

public class KyivClamPizza extends KyivPizza implements ClamPizza {

    public void prepare() {
        System.out.println("prepare Kyiv clam pizza");
    }

    public void bake() {
        System.out.println("bake Kyiv clam pizza");
    }

}
