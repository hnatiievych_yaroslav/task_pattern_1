package com.hnatiievych.model.impl.kyiv;

import com.hnatiievych.model.Pizza;
import com.hnatiievych.model.Toppings;

import java.util.List;

public abstract class KyivPizza implements Pizza {
    private String dough;
    private String sauce;
    List<Toppings> toppings;

    public abstract void prepare();

    public abstract void bake();

    public void cut() {
        System.out.println("cut kyiv pizza");
    }

    public void box() {
        System.out.println("box kyiv  pizza");
    }
}
