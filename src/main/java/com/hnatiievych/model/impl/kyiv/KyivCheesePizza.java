package com.hnatiievych.model.impl.kyiv;

import com.hnatiievych.model.CheesePizza;

public class KyivCheesePizza extends KyivPizza implements CheesePizza {

    public void prepare() {
        System.out.println("prepare Kyiv cheese pizza");
    }

    public void bake() {
        System.out.println("bake Kyiv cheese pizza");
    }

}
