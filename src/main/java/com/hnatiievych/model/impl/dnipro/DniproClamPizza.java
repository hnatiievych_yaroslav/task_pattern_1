package com.hnatiievych.model.impl.dnipro;

import com.hnatiievych.model.ClamPizza;

public class DniproClamPizza extends DniproPizza implements ClamPizza {

    public void prepare() {
        System.out.println("prepare Dnipro clam pizza");
    }

    public void bake() {
        System.out.println("bake Dnipro clam pizza");
    }

}
