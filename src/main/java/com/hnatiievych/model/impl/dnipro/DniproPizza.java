package com.hnatiievych.model.impl.dnipro;

import com.hnatiievych.model.Pizza;
import com.hnatiievych.model.Toppings;

import java.util.List;

public abstract class DniproPizza implements Pizza {
    private String dough;
    private String sauce;
    List<Toppings> toppings;

    public abstract void prepare();

    public abstract void bake();

    public void cut() {
        System.out.println("cut dnipro  pizza");
    }

    public void box() {
        System.out.println("box dnipro  pizza");
    }
}
