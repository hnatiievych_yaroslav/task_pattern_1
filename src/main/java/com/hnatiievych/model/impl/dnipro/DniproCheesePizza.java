package com.hnatiievych.model.impl.dnipro;

import com.hnatiievych.model.CheesePizza;

public class DniproCheesePizza extends DniproPizza implements CheesePizza {

    public void prepare() {
        System.out.println("prepare dnipro cheese pizza");
    }

    public void bake() {
        System.out.println("bake dnipro cheese pizza");
    }

}
