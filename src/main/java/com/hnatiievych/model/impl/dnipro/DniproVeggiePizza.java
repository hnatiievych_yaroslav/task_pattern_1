package com.hnatiievych.model.impl.dnipro;

import com.hnatiievych.model.VeggiePizza;

public class DniproVeggiePizza extends DniproPizza implements VeggiePizza {

    public void prepare() {
        System.out.println("prepare Dnipro veggie pizza");
    }

    public void bake() {
        System.out.println("bake Dnipro veggie pizza");
    }

}
