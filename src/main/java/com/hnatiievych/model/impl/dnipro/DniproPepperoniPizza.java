package com.hnatiievych.model.impl.dnipro;

import com.hnatiievych.model.PepperoniPizza;

public class DniproPepperoniPizza extends DniproPizza implements PepperoniPizza {

    public void prepare() {
        System.out.println("prepare Dnipro pepperoni pizza");
    }

    public void bake() {
        System.out.println("bake Dnipro pepperoni pizza");
    }

}
