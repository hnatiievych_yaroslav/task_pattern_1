package com.hnatiievych.model;

import java.util.Arrays;

public enum City {
    LVIV("Lviv"),DNIPRO("Dnipro"),KYIV("Kyiv");
    private String name;

    City(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
