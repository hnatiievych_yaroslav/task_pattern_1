package com.hnatiievych.model;

import com.hnatiievych.model.factory.DniproPizzaFactory;
import com.hnatiievych.model.factory.KyivPizzaFactory;
import com.hnatiievych.model.factory.LvivPizzaFactory;
import com.hnatiievych.model.factory.PizzaFactory;
import com.hnatiievych.model.impl.lviv.LvivCheesePizza;
import com.hnatiievych.model.impl.lviv.LvivClamPizza;
import com.hnatiievych.model.impl.lviv.LvivPepperoniPizza;
import com.hnatiievych.model.impl.lviv.LvivVeggiePizza;

import static com.hnatiievych.model.City.*;
import static com.hnatiievych.model.PizzaType.*;

public class PizzaConfig {
    public static Pizza choosePizza(City city, String pizzaType) {
        switch (city) {
            case LVIV:

                if (CHEESE.getName().equals(pizzaType)) {
                    return new LvivPizzaFactory().cheesePizza();
                } else if (VEGGIE.getName().equals(pizzaType)) {
                    return new LvivPizzaFactory().veggiePizza();
                } else if (CLAM.getName().equals(pizzaType)) {
                    return new LvivPizzaFactory().clamPizza();
                } else if (PEPPERONI.getName().equals(pizzaType)) {
                    return new LvivPizzaFactory().pepperoniPizza();
                }
                throw new RuntimeException("Can`t found Pizza type to create.");

            case KYIV:
                if (CHEESE.getName().equals(pizzaType)) {
                    return new KyivPizzaFactory().cheesePizza();
                } else if (VEGGIE.getName().equals(pizzaType)) {
                    return new KyivPizzaFactory().veggiePizza();
                } else if (CLAM.getName().equals(pizzaType)) {
                    return new KyivPizzaFactory().clamPizza();
                } else if (PEPPERONI.getName().equals(pizzaType)) {
                    return new KyivPizzaFactory().pepperoniPizza();
                }
                throw new RuntimeException("Can`t found Pizza type to create.");

            case DNIPRO:
                if (CHEESE.getName().equals(pizzaType)) {
                    return new DniproPizzaFactory().cheesePizza();
                } else if (VEGGIE.getName().equals(pizzaType)) {
                    return new DniproPizzaFactory().veggiePizza();
                } else if (CLAM.getName().equals(pizzaType)) {
                    return new DniproPizzaFactory().clamPizza();
                } else if (PEPPERONI.getName().equals(pizzaType)) {
                    return new DniproPizzaFactory().pepperoniPizza();
                }
                throw new RuntimeException("Can`t found Pizza type to create.");
            default:
                throw new RuntimeException("Can`t found city.");
        }
    }
}
