package com.hnatiievych.model;

public enum PizzaType {
    CHEESE("CHEESE"), VEGGIE("VEGGIE"), CLAM("CLAM"), PEPPERONI("PEPPERONI");
    private String name;

    private PizzaType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
